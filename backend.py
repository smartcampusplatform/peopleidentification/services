from flask import Flask, render_template, redirect, request, jsonify
import sqlite3
import os.path
import requests

#from dotenv import load_dotenv
#load_dotenv()

app = Flask(__name__)

#db_server=os.getenv("DATABASE")
db_server='https://dbreksti.herokuapp.com/'
#db_server='http://127.0.0.1:8050/' 

#service_server=os.getenv("SERVICE")
service_server='https://servicereksti.herokuapp.com/'
#service_server='http://127.0.0.1:5000/' 

accessLevel_to_category={}
accessLevel_to_category["1"]="mahasiswa"
accessLevel_to_category["2"]="dosen"
accessLevel_to_category["3"]="tamu"
accessLevel_to_category["4"]="grup"

@app.route('/sendTapData')
def sendData():
#	try:
	cardID =  request.args.get('cardID')
	locationID = request.args.get('locationID')
	timestamp = request.args.get('timestamp')
	alert = request.args.get('alert')
	
	url=service_server+'verifyAccess?cardID="{}"&locationID="{}";'.format(cardID,locationID)
	
	#valid=bool(str(verifyAccess(cardID,locationID).data)[3:-4])
	valid=verifyAccess(cardID,locationID)
		
	url=db_server+'insert?query=INSERT INTO Tap VALUES ("{}","{}","{}","{}","{}");'.format(cardID,locationID,timestamp,valid,alert)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	r = requests.get(url,headers=headers)
	
	return str(valid)
#	except:
#		return "Failed"
		
@app.route('/getTapData')
def getTapData():
#	try:
	url=db_server+'select?query=select Location.locationID,location.Name,Tap.timestamp,Card.cardID,User.userID,User.name,Tap.valid,Tap.alert from Tap,Card,User,Location where Tap.cardID=Card.cardID and Card.userID=User.userID and Tap.locationID=Location.locationID'
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	data = requests.get(url,headers=headers).json()
	
	return jsonify(data)
#	except:
#		return "Failed"

@app.route('/getUserData')
def getUserData():
#	try:
		
	url=db_server+'select?query=select * from User;'
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	data = requests.get(url,headers=headers).json()
	
	return jsonify(data)
#	except:
#		return "Failed"

@app.route('/getUserCard')
def getUserCard():
#	try:
	userID =  request.args.get('userID')
	
	url=db_server+'select?query=select cardID from Card where userID="{}"'.format(userID)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	data = requests.get(url,headers=headers).json()
	
	return jsonify(data[0][0])
#	except:
#		return "Failed"

@app.route('/getLocationData')
def getLocationData():
#	try:
	url=db_server+'select?query=select * from Location;'
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	data = requests.get(url,headers=headers).json()
	
	return jsonify(data)
#	except:
#		return "Failed"
				
#request udah bener tinggal processing
@app.route('/verifyAccess')
def verifyAccess(cardID=None,locationID=None):
#	try:
	cardID =  request.args.get('cardID')
	locationID = request.args.get('locationID')
	
	url=db_server+'select?query=SELECT userID FROM Card Where cardID="{}" ;'.format(cardID)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	data = requests.get(url,headers=headers).json()
	userID = data[0][0]
	
	url=db_server+'select?query=SELECT accessLevel FROM User Where userID="{}" ;'.format(userID)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	data = requests.get(url,headers=headers).json()
	accessLevel = data[0][0]
		
	url=db_server+'select?query=SELECT locationID FROM AccessCategory Where accessLevel="{}" ;'.format(accessLevel)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	data = requests.get(url,headers=headers).json()
	legal_location = [value[0] for value in data]
	
	legal=locationID in legal_location
	
	if legal :
		return "True"

	else :
		return "False"
#	except:
#		return "Failed"
			
@app.route('/addUser')
def addUser():
#	try:
	userID =  request.args.get('userID')
	name = request.args.get('name')
	accessLevel = request.args.get('accessLevel')
	category = accessLevel_to_category[accessLevel]
	institution = request.args.get('institution')
	specialAccess = "False"	
	
	url=db_server+'insert?query=INSERT INTO User VALUES ("{}","{}","{}","{}","{}","{}");'.format(userID,name,category,accessLevel,institution,specialAccess)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	r = requests.get(url,headers=headers)
	
	return "Success"
		
#	except:
#		return "Failed"
		
@app.route('/addCard')
def addCard():
#	try:
	cardID =  request.args.get('cardID')
	
	url=db_server+'insert?query=INSERT INTO Card VALUES ("{}","None");'.format(cardID)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	r = requests.get(url,headers=headers)
		
	return "Success"
		
#	except:
#		return "Failed"
		
@app.route('/grantAccess')
def grantAccess():
#	try:
	userID =  request.args.get('userID')
	locationID = request.args.get('locationID')
	
	url=db_server+'insert?query=INSERT INTO SpecialAccess VALUES ("{}","{}");'.format(userID,locationID)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	r = requests.get(url,headers=headers)
	
	return "Success"
#	except:
#		return "Failed"
		
@app.route('/assignCard')
def assignCard():
#	try:
	cardID =  request.args.get('cardID')	
	userID =  request.args.get('userID')
	
	url=db_server+'update?query=UPDATE Card SET userID="{}" WHERE cardID="{}";'.format(userID,cardID)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	r = requests.get(url,headers=headers)
	
	return "Success"
	
@app.route('/returnCard')
def returnCard():
#	try:
	userID =  request.args.get('userID')
	
	url=db_server+'update?query=UPDATE Card SET userID="None" WHERE userID="{}";'.format(userID)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	r = requests.get(url,headers=headers)
	
	return "Success"
	
#	except:
#		return "Failed"
if __name__ == '__main__':
    app.run()
