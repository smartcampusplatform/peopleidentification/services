Readme untuk menjalankan services People Identification

Beberapa api sederhana yang tersimpan di database (menggunakan domain heroku):
- Send dan get data tapping RFID
- Melakukan verifikasi akses data ke database untuk memperoleh otorisasi ruangan
- Mengambil data-data dari database
- Melakukan kontrol terhadap frontend tamu


1. Send dan get
    API Get: /getTapData
    API Send: /sendTapData

2. Verifikasi akses
    API : /verifyAccess

3. Proses Database
    API : /getUserData & /getUserCard & /getLocationData

4. Kontrol frontend
    API : /grantAccess & /assignCard & /addCard & /addUser


Untuk sementara .env tidak dapat digunakan karena terjadi error yang belum terselesaikan.

Untuk mengganti alamat server database dan service, dimohon untuk mengganti variabel :
- db_server : isi dengan alamat server database
- service_server : isi dengan alamat server service

Untuk menjalankan services, ketik perintah "python backend.py" pada command line